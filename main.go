package main

import (
	"context"
	"errors"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
	"net/http"
)

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
type Response events.APIGatewayProxyResponse

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context) error {
	client := http.DefaultClient

	rt := WithHeader(client.Transport)
	rt.Add("x-api-key", "s4bux1YtpafyTbX33qH47cmLzhTqt0H3SxesTXb2")
	rt.Add("Authorization", "Bearer "+"")
	client.Transport = rt

	_, err := http.Post("https://xotuivfggd.execute-api.us-east-1.amazonaws.com/dev/accounts/users/cron-temporary-permission", "application/json", nil)
	if err != nil {
		return errors.New("could not connect to the endpoint")
	}

	log.Println("Cron ejecutado correctamente")

	return nil
}

type withHeader struct {
	http.Header
	rt http.RoundTripper
}

func WithHeader(rt http.RoundTripper) withHeader {
	if rt == nil {
		rt = http.DefaultTransport
	}

	return withHeader{Header: make(http.Header), rt: rt}
}

func (h withHeader) RoundTrip(req *http.Request) (*http.Response, error) {
	for k, v := range h.Header {
		req.Header[k] = v
	}

	return h.rt.RoundTrip(req)
}

func main() {
	lambda.Start(Handler)
	log.Println("Cron ejecutado correctamente")
}
